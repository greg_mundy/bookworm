package edu.ab.bookworm.domain;

public class Review {
    private Long id;
    private String review;
    private Reviewer reviewer;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getReview() {
	return review;
    }

    public void setReview(String review) {
	this.review = review;
    }

    public Reviewer getReviewer() {
	return reviewer;
    }

    public void setReviewer(Reviewer reviewer) {
	this.reviewer = reviewer;
    }
}
