package edu.ab.bookworm.domain;

import java.util.List;

public class Book {
    private String isbn;
    private String title;
    private Long yearPublished;
    private List<Author> authors;
    private List<Review> reviews;

    public String getIsbn() {
	return isbn;
    }

    public void setIsbn(String isbn) {
	this.isbn = isbn;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public Long getYearPublished() {
	return yearPublished;
    }

    public void setYearPublished(Long yearPublished) {
	this.yearPublished = yearPublished;
    }

    public List<Author> getAuthors() {
	return authors;
    }

    public void setAuthors(List<Author> authors) {
	this.authors = authors;
    }

    public List<Review> getReviews() {
	return reviews;
    }

    public void setReviews(List<Review> reviews) {
	this.reviews = reviews;
    }
}
