package edu.ab.bookworm.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConnectionUtil {
    private static final String CONNECTION_URI = "jdbc:mysql://54.245.71.7:3306/bookworm";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "1greg1";

    public static final Connection getConnection() {
	Connection connection = null;
	try {
	    connection = DriverManager.getConnection(CONNECTION_URI, USERNAME,
		    PASSWORD);
	} catch (SQLException e) {
	    System.out.println("Unable to connect to database "
		    + CONNECTION_URI);
	}
	return connection;
    }

    public static final Boolean closeConnection(Connection connection) {
	Boolean isClosed = false;
	try {
	    if (!connection.isClosed()) {
		connection.close();
		isClosed = true;
	    }
	} catch (SQLException e) {
	    System.out.println("Failed to close database connection...");
	}
	return isClosed;
    }

    public static final Statement getStatement(Connection connection) {
	Statement statement = null;
	try {
	    statement = connection.createStatement();
	} catch (SQLException e) {
	    System.out.println("Unable to create statement");
	}
	return statement;
    }

    public static final void executeQuery(String query) {
	Connection connection = getConnection();
	Statement statement = getStatement(connection);
	try {
	    statement.execute(query);
	} catch (SQLException e) {
	    System.out.println("Unable to execute query -> " + query);
	}
    }

    public static final void createTables() {
	String authorTableQuery = "CREATE TABLE IF NOT EXISTS author("
		+ "id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,"
		+ "first_name VARCHAR(20), last_name VARCHAR(20))";

	String bookTableQuery = "CREATE TABLE IF NOT EXISTS book("
		+ "isbn VARCHAR(20) PRIMARY KEY NOT NULL,"
		+ "title VARCHAR(50), year_published INTEGER)";

	String reviewerTableQuery = "CREATE TABLE IF NOT EXISTS reviewer("
		+ "id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,"
		+ "first_name VARCHAR(20), last_name VARCHAR(20))";

	String reviewTableQuery = "CREATE TABLE IF NOT EXISTS review("
		+ "id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,"
		+ "review VARCHAR(255), reviewer_id INTEGER,"
		+ "FOREIGN KEY(reviewer_id) REFERENCES reviewer(id))";

	String authorBookTableQuery = "CREATE TABLE IF NOT EXISTS author_book("
		+ "author_id INTEGER NOT NULL," + "book_id INTEGER NOT NULL,"
		+ "PRIMARY KEY(author_id, book_id),"
		+ "FOREIGN KEY(author_id) REFERENCES author(id),"
		+ "FOREIGN KEY(book_id) REFERENCES book(id))";

	executeQuery(authorTableQuery);
	executeQuery(bookTableQuery);
	executeQuery(reviewerTableQuery);
	executeQuery(reviewTableQuery);
	executeQuery(authorBookTableQuery);
    }

    public static final void deleteTables() {
	List<String> tables = new ArrayList<String>();
	tables.add("author");
	tables.add("book");
	tables.add("reviewer");
	tables.add("review");
	tables.add("author_book");
	String dropTableQuery = "DROP TABLE IF EXISTS ";
	
	for (String table : tables) {
	    executeQuery(dropTableQuery + table);
	}
    }
}