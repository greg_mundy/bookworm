package edu.ab.bookworm.data;

import java.util.List;

public interface DataAccessObject<T> {
    public void save(T item);

    public T findOne(Long id);

    public List<T> findAll();

    public void update(T item);

    public void delete(T item);

    public Integer count();
}
