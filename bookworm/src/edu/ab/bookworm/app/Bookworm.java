package edu.ab.bookworm.app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import edu.ab.bookworm.data.ConnectionUtil;

public class Bookworm {
    private static final Integer MINIMUM_OPTION = 1;
    private static final Integer MAXIMUM_OPTION = 10;
    private static Scanner scanner = new Scanner(System.in);

    private static void getMenu() {
	System.out.println("*** BOOKWORM ***");
	System.out.println("1. Add New Book");
	System.out.println("2. Edit Existing Book");
	System.out.println("3. View Book Details");
	System.out.println("4. Delete Book");
	System.out.println("5. List All Books");
	System.out.println("6. Find Books By Author");
	System.out.println("7. Find Books By ISBN");
	System.out.println("8. Write Book Review");
	System.out.println("9. Read Reviews For Book");
	System.out.println("10. Exit");
    }

    private static void addBook() {
	System.out.println("== Add Book ==");
	String isbn;
	String title;
	String yearPublished;

	scanner.nextLine();
	System.out.print("ISBN => ");
	isbn = scanner.nextLine();

	System.out.print("TITLE => ");
	title = scanner.nextLine();

	System.out.print("YEAR PUBLISHED => ");
	yearPublished = scanner.nextLine();

	String addBookStmt = "INSERT INTO book VALUES('" + isbn + "','" + title
		+ "'," + new Long(yearPublished) + ")";

	ConnectionUtil.executeQuery(addBookStmt);
    }

    private static void deleteBook() {
	System.out.println("== Delete Book ==");
	String isbn;
	scanner.nextLine();
	System.out.print("ISBN of book to delete ==> ");
	isbn = scanner.nextLine();

	String deleteBookStmt = "DELETE FROM book WHERE isbn = '" + isbn + "'";
	ConnectionUtil.executeQuery(deleteBookStmt);

    }

    private static void editBook() {
	System.out.println("== Edit Book ==");
	String isbn;
	String updatedTitle;
	String updatedYearPublished;
	scanner.nextLine();
	System.out.print("ISBN of book to edit ==> ");
	isbn = scanner.nextLine();
	String lookupBookQuery = "SELECT * FROM book WHERE isbn = '" + isbn
		+ "'";

	Connection connection = ConnectionUtil.getConnection();
	Statement statement = ConnectionUtil.getStatement(connection);
	try {
	    ResultSet resultSet = statement.executeQuery(lookupBookQuery);
	    resultSet.next();
	    String foundISBN = resultSet.getString("isbn");
	    String foundTitle = resultSet.getString("title");
	    String foundYearPublished = resultSet.getString("year_published");

	    System.out.println("ISBN: " + foundISBN);
	    System.out.println("Title: " + foundTitle);
	    System.out.println("Year Published: " + foundYearPublished);

	    System.out
		    .print("Enter new title (or leave blank for no change) => ");
	    updatedTitle = scanner.nextLine();

	    System.out
		    .print("Enter new publication year (or leave blank for no change) => ");
	    updatedYearPublished = scanner.nextLine();

	    if (updatedTitle == "") {
		updatedTitle = foundTitle;
	    }

	    if (updatedYearPublished == "") {
		updatedYearPublished = foundYearPublished;
	    }

	    String updateBookStmt = "UPDATE book SET title = '" + updatedTitle
		    + "', year_published = " + new Long(updatedYearPublished)
		    + " WHERE isbn = '" + isbn + "'";
	    ConnectionUtil.executeQuery(updateBookStmt);
	} catch (SQLException e) {
	    System.out.println("Unable to locate book with ISBN => " + isbn);
	} finally {
	    ConnectionUtil.closeConnection(connection);
	}
    }

    private static void viewBook() {
	System.out.println("== View Book ==");
	String isbn;
	scanner.nextLine();
	System.out.print("ISBN to look up ==> ");
	isbn = scanner.nextLine();
	String lookupBookQuery = "SELECT * FROM book WHERE isbn = '" + isbn
		+ "'";

	Connection connection = ConnectionUtil.getConnection();
	Statement statement = ConnectionUtil.getStatement(connection);
	try {
	    ResultSet resultSet = statement.executeQuery(lookupBookQuery);
	    resultSet.next();
	    String foundISBN = resultSet.getString("isbn");
	    String foundTitle = resultSet.getString("title");
	    String foundYearPublished = resultSet.getString("year_published");

	    System.out.println("ISBN: " + foundISBN);
	    System.out.println("Title: " + foundTitle);
	    System.out.println("Year Published: " + foundYearPublished);
	} catch (SQLException e) {
	    System.out.println("Unable to locate book with ISBN => " + isbn);
	} finally {
	    ConnectionUtil.closeConnection(connection);
	}
    }

    private static void listAllBooks() {
	System.out.println("== List All Books ==");

	String lookupBookQuery = "SELECT * FROM book";

	Connection connection = ConnectionUtil.getConnection();
	Statement statement = ConnectionUtil.getStatement(connection);
	try {
	    ResultSet resultSet = statement.executeQuery(lookupBookQuery);
	    while (resultSet.next()) {
		String foundISBN = resultSet.getString("isbn");
		String foundTitle = resultSet.getString("title");
		String foundYearPublished = resultSet
			.getString("year_published");

		System.out.println("ISBN: " + foundISBN);
		System.out.println("Title: " + foundTitle);
		System.out.println("Year Published: " + foundYearPublished);
	    }
	} catch (SQLException e) {
	    System.out.println("Unable to locate books!");
	} finally {
	    ConnectionUtil.closeConnection(connection);
	}
    }

    public static Integer getOption() {
	Boolean validOption = false;
	Integer option = 0;
	do {
	    System.out.print("Select an option ==> ");
	    try {
		option = scanner.nextInt();
		if (option >= MINIMUM_OPTION && option <= MAXIMUM_OPTION) {
		    validOption = true;
		} else {
		    System.out.println("[ERROR] Selection must be between "
			    + MINIMUM_OPTION + " and " + MAXIMUM_OPTION);
		}
	    } catch (InputMismatchException e) {
		System.out.println("[ERROR] Input must be an integer!");
		scanner.next();
	    }
	} while (!validOption);
	return option;
    }

    public static void main(String... args) {
	ConnectionUtil.createTables();
	Integer menuOption = 0;
	do {
	    getMenu();
	    menuOption = getOption();
	    switch (menuOption) {
	    case 1:
		addBook();
		break;
	    case 2:
		editBook();
		break;
	    case 3:
		viewBook();
		break;
	    case 4:
		deleteBook();
		break;
	    case 5:
		listAllBooks();
		break;
	    case 10:
		System.out.println("Thank you for using BookWorm!");
		break;
	    default:
		System.out.println("[Error] An invalid option was selected!");
	    }
	} while (menuOption != MAXIMUM_OPTION);
    }
}
