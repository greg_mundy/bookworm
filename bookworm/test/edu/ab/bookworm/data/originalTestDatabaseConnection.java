package edu.ab.bookworm.data;

import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Ignore;
import org.junit.Test;

public class originalTestDatabaseConnection {

    @Test
    public void testMySQLConnection() {
	Connection connection = null;
	try {
	    connection = DriverManager.getConnection(CONNECTION_URI, USERNAME,
		    PASSWORD);
	} catch (SQLException e) {
	    e.printStackTrace();
	}
	assertNotNull(connection);

	try {
	    if (!connection.isClosed()) {
		connection.close();
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	}
    }

    @Test
    public void testCreateTables() {
	ConnectionUtil.createTables();
    }

    @Test
    @Ignore
    public void testDeleteTables() {
	ConnectionUtil.deleteTables();
    }

    private static final String CONNECTION_URI = "jdbc:mysql://54.245.71.7:3306/bookworm";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "1greg1";

}
