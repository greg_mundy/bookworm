package edu.ab.bookworm.data;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;

public class TestDatabaseConnection {

    @Test
    public void testMySQLConnection() {
	Connection connection = null;

	try {
	    connection = DriverManager.getConnection(CONNECTION_URI, USERNAME,
		    PASSWORD);
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	assertNotNull(connection);

	try {
	    if (!connection.isClosed()) {
		connection.close();
	    }
	    assertTrue(connection.isClosed());
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }
    
    @Test
    public void testCreateTables() {
	ConnectionUtil.createTables();
    }

    private static final String CONNECTION_URI = "jdbc:mysql://54.245.71.7:3306/bookworm";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "1greg1";
}
